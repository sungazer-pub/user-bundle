#! /bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PACKAGE="sungazer/user-bundle"

if [ "$1" = "set" ]; then
	composer config -g "repositories.$PACKAGE" vcs "$DIR"
elif [ "$1" = "unset" ]; then
	composer config -g --unset "repositories.$PACKAGE"
else
	echo "Usage: composer-repository.sh (set|unset)"
fi
