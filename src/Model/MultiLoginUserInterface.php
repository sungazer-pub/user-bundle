<?php


namespace Sungazer\Bundle\UserBundle\Model;


use Symfony\Component\Security\Core\User\UserInterface;

interface MultiLoginUserInterface extends UserInterface
{
    /**
     * @return string
     */
    public function getId();

    /**
     * @return string
     */
    public function getLoginUsername();

    /**
     * @return string
     */
    public function getLoginEmail();

    /**
     * @return string
     */
    public function getLoginPhone();

    /**
     * @param string $email
     * @return $this
     */
    public function setLoginEmail(string $email);

    /**
     * @param string $phone
     * @return $this
     */
    public function setLoginPhone(string $phone);

    /**
     * @param string $username
     * @return $this
     */
    public function setLoginUsername(string $username);

    /**
     * @return bool
     */
    public function isLoginPhoneValidated();

    /**
     * @param bool $valid
     * @return $this
     */
    public function setLoginPhoneValidated(bool $valid);

    /**
     * @return bool
     */
    public function isLoginEmailValidated();

    /**
     * @param bool $valid
     * @return $this
     */
    public function setLoginEmailValidated(bool $valid);

    /**
     * @param string | null $password
     * @return $this
     */
    public function setPassword(?string $password);

}