<?php


namespace Sungazer\Bundle\UserBundle\Model\Token;


use DateInterval;
use DateTime;
use DateTimeInterface;
use Sungazer\Bundle\UserBundle\Model\TokenInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class ExpirableToken implements TokenInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="text")
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(type="guid")
     * @var string
     */
    protected $userId;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTimeInterface
     */
    protected $expiresAt;
    /**
     * @ORM\Column(type="datetime")
     * @var DateTimeInterface
     */
    protected $createdAt;

    public function __construct($userId = null, string $intervalSpec = "PT1H")
    {
        $this->createdAt = new DateTime('now');
        $this->expiresAt = clone $this->createdAt;
        $this->userId    = $userId;
        $this->expiresAt->add(new DateInterval($intervalSpec));
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId(string $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return $this
     */
    public function setUserId(string $userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new DateTime('now');
    }

    public function isExpired(): bool
    {
        $now = new DateTime('now');
        return $now > $this->expiresAt;
    }

    /**
     * @param string $token
     * @return $this
     */
    public function setToken(string $token)
    {
        $this->id = $token;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->id;
    }
}