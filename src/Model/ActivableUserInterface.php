<?php


namespace Sungazer\Bundle\UserBundle\Model;


use Symfony\Component\Security\Core\User\UserInterface;

interface ActivableUserInterface extends UserInterface
{
    /**
     * @return bool
     */
    public function getActive(): bool;

}