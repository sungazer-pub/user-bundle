<?php


namespace Sungazer\Bundle\UserBundle\Model;


interface TokenInterface
{
    /**
     * @param string $token
     * @return $this
     */
    public function setToken(string $token);

    /**
     * @return string
     */
    public function getToken();

}