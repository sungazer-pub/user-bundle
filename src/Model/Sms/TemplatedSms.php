<?php


namespace Sungazer\Bundle\UserBundle\Model\Sms;


class TemplatedSms extends Sms
{
    /**
     * @var string
     */
    private $textTemplate;

    /**
     * @var array
     */
    private $context;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string
     */
    public function getTextTemplate(): string
    {
        return $this->textTemplate;
    }

    /**
     * @param string $textTemplate
     * @return TemplatedSms
     */
    public function setTextTemplate(string $textTemplate): TemplatedSms
    {
        $this->textTemplate = $textTemplate;
        return $this;
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }

    /**
     * @param array $context
     * @return TemplatedSms
     */
    public function setContext(array $context): TemplatedSms
    {
        $this->context = $context;
        return $this;
    }
}