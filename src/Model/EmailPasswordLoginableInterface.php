<?php


namespace Sungazer\Bundle\UserBundle\Model;


use Symfony\Component\Security\Core\User\UserInterface;

interface EmailPasswordLoginableInterface extends UserInterface
{
    /**
     * @return string
     */
    public function getId();

    /**
     * @return string
     */
    public function getLoginEmail();

    /**
     * @param string $email
     * @return $this
     */
    public function setLoginEmail(string $email);

    /**
     * @return bool
     */
    public function isLoginEmailValidated();

    /**
     * @param bool $valid
     * @return $this
     */
    public function setLoginEmailValidated(bool $valid);

    public function getPassword();

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password);
}