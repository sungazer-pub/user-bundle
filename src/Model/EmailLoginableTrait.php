<?php


namespace Sungazer\Bundle\UserBundle\Model;

use Doctrine\ORM\Mapping as ORM;

trait EmailLoginableTrait
{
    /**
     * @var string
     * @ORM\Column(type="text",nullable=true, unique=true)
     */
    private $loginEmail;
    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false, options={"default":false})
     */
    private $loginEmailValidated = false;

    /**
     * @return string | null
     */
    public function getLoginEmail()
    {
        return $this->loginEmail;
    }

    /**
     * @param string | null $loginEmail
     * @return $this
     */
    public function setLoginEmail(?string $loginEmail)
    {
        $this->loginEmail = $loginEmail;
        return $this;
    }

    /**
     * @return bool
     */
    public function isLoginEmailValidated()
    {
        return $this->loginEmailValidated;
    }

    /**
     * @param bool $loginEmailValidated
     * @return $this
     */
    public function setLoginEmailValidated(bool $loginEmailValidated)
    {
        $this->loginEmailValidated = $loginEmailValidated;
        return $this;
    }
}