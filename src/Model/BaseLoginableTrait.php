<?php


namespace Sungazer\Bundle\UserBundle\Model;

use Doctrine\ORM\Mapping as ORM;

trait BaseLoginableTrait
{
    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $password;
    /**
     * @var array
     * @ORM\Column(type="json",nullable=true)
     */
    private $roles = [];


    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function addRole(string $role)
    {
        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }
        return $this;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function removeRole(string $role)
    {
        $key = array_search($role, $this->roles);
        if ($key) {
            unset($this->roles[$key]);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string | null $password
     * @return $this
     */
    public function setPassword(?string $password)
    {
        $this->password = $password;
        return $this;
    }

    // Not needed for our class
    public function getSalt(){
        return null;
    }

    // Not needed for our class
    public function eraseCredentials(){
        return null;
    }
}