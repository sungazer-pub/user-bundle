<?php


namespace Sungazer\Bundle\UserBundle\Model;

use Doctrine\ORM\Mapping as ORM;

trait ActivableUserTrait
{

    /**
     * @var boolean
     * @ORM\Column(type="boolean",nullable=false, options={"default": true})
     */
    private $active = true;

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return $this
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
        return $this;
    }


}