<?php


namespace Sungazer\Bundle\UserBundle\Model;

use Doctrine\ORM\Mapping as ORM;

trait MultiLoginUserTrait
{

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true, unique=true)
     */
    private $loginEmail;
    /**
     * @var string
     * @ORM\Column(type="text",nullable=true, unique=true)
     */
    private $loginPhone;
    /**
     * @var string
     * @ORM\Column(type="text",nullable=true, unique=true)
     */
    private $loginUsername;
    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false, options={"default":false})
     */
    private $loginPhoneValidated = false;
    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false, options={"default":false})
     */
    private $loginEmailValidated = false;
    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $password;
    /**
     * @var array
     * @ORM\Column(type="json",nullable=true)
     */
    private $roles = [];


    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function addRole(string $role)
    {
        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }
        return $this;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function removeRole(string $role)
    {
        $key = array_search($role, $this->roles);
        if ($key) {
            unset($this->roles[$key]);
        }
        return $this;
    }

    /**
     * @return string | null
     */
    public function getLoginUsername()
    {
        return $this->loginUsername;
    }

    /**
     * @param string | null $loginUsername
     * @return $this
     */
    public function setLoginUsername(?string $loginUsername)
    {
        $this->loginUsername = $loginUsername;
        return $this;
    }

    /**
     * @return string | null
     */
    public function getLoginEmail()
    {
        return $this->loginEmail;
    }

    /**
     * @param string | null $loginEmail
     * @return $this
     */
    public function setLoginEmail(?string $loginEmail)
    {
        $this->loginEmail = $loginEmail;
        return $this;
    }

    /**
     * @return string | null
     */
    public function getLoginPhone()
    {
        return $this->loginPhone;
    }

    /**
     * @param string | null $loginPhone
     * @return $this
     */
    public function setLoginPhone(?string $loginPhone)
    {
        $this->loginPhone = $loginPhone;
        return $this;
    }

    /**
     * @return bool
     */
    public function isLoginPhoneValidated()
    {
        return $this->loginPhoneValidated;
    }

    /**
     * @param bool $loginPhoneValidated
     * @return $this
     */
    public function setLoginPhoneValidated(bool $loginPhoneValidated)
    {
        $this->loginPhoneValidated = $loginPhoneValidated;
        return $this;
    }

    /**
     * @return bool
     */
    public function isLoginEmailValidated()
    {
        return $this->loginEmailValidated;
    }

    /**
     * @param bool $loginEmailValidated
     * @return $this
     */
    public function setLoginEmailValidated(bool $loginEmailValidated)
    {
        $this->loginEmailValidated = $loginEmailValidated;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string | null $password
     * @return $this
     */
    public function setPassword(?string $password)
    {
        $this->password = $password;
        return $this;
    }

    // Not needed for our class
    public function getSalt(){
        return null;
    }

    // Not needed for our class
    public function eraseCredentials(){
        return null;
    }

    // Utility override
    public function getUsername(){
        return $this->getLoginEmail() ?? $this->getLoginPhone() ?? $this->getLoginUsername();
    }


}