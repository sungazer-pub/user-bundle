<?php


namespace Sungazer\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sungazer\Bundle\UserBundle\Model\Token\ExpirableToken;

/**
 * @ORM\Entity()
 * @ORM\Table(name="sungazer_user_password_reset_token")
 * @ORM\HasLifecycleCallbacks()
 */
class PasswordResetToken extends ExpirableToken
{
    public function __construct($userId = null, string $intervalSpec = "PT1H")
    {
        parent::__construct($userId, $intervalSpec);
    }
}