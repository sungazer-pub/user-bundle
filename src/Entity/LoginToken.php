<?php


namespace Sungazer\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sungazer\Bundle\UserBundle\Model\Token\ExpirableToken;

/**
 * @ORM\Entity()
 * @ORM\Table(name="sungazer_user_login_token")
 * @ORM\HasLifecycleCallbacks
 */
class LoginToken extends ExpirableToken
{

    const DELIVERY_EMAIL = "email";
    const DELIVERY_PHONE = "phone";

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $deliveryMethod;

    public function __construct($userId = null, string $intervalSpec = "PT1H")
    {
        parent::__construct($userId, $intervalSpec);
    }

    /**
     * @return string
     */
    public function getDeliveryMethod(): string
    {
        return $this->deliveryMethod;
    }

    /**
     * @param string $deliveryMethod
     * @return LoginToken
     */
    public function setDeliveryMethod(string $deliveryMethod): LoginToken
    {
        $this->deliveryMethod = $deliveryMethod;
        return $this;
    }

}