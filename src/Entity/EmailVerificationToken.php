<?php


namespace Sungazer\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sungazer\Bundle\UserBundle\Model\Token\ExpirableToken;

/**
 * @ORM\Entity()
 * @ORM\Table(name="sungazer_user_email_verification_token")
 * @ORM\HasLifecycleCallbacks()
 */
class EmailVerificationToken extends ExpirableToken
{
    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $email;

    public function __construct($userId = null, string $intervalSpec = "PT1H")
    {
        parent::__construct($userId, $intervalSpec);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return EmailVerificationToken
     */
    public function setEmail(string $email): EmailVerificationToken
    {
        $this->email = $email;
        return $this;
    }
}