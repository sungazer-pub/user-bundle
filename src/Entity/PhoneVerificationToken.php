<?php


namespace Sungazer\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sungazer\Bundle\UserBundle\Model\Token\ExpirableToken;

/**
 * @ORM\Entity()
 * @ORM\Table(name="sungazer_user_phone_verification_token")
 * @ORM\HasLifecycleCallbacks
 */
class PhoneVerificationToken extends ExpirableToken
{
    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $phone;

    public function __construct($userId = null, string $intervalSpec = "PT1H")
    {
        parent::__construct($userId, $intervalSpec);
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return PhoneVerificationToken
     */
    public function setPhone(string $phone): PhoneVerificationToken
    {
        $this->phone = $phone;
        return $this;
    }
}