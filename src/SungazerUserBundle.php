<?php

namespace Sungazer\Bundle\UserBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/*
 * Gather common utilities for user signup, authentication etc...
 *
 * Author: Luca Nardelli <luca@sungazer.io>
 */

class SungazerUserBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }
}

