<?php


namespace Sungazer\Bundle\UserBundle\Service;


use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sungazer\Bundle\UserBundle\Model\MultiLoginUserInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserManager
{

    /**
     * @var string
     */
    private $userClass;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(string $userClass, EntityManagerInterface $em)
    {
        $this->userClass     = $userClass;
        $this->entityManager = $em;
    }

    public function createUser()
    {
        return new $this->userClass();
    }

    public function getUserClass(): ?string
    {
        return $this->userClass;
    }

    /**
     * @return ObjectRepository
     */
    public function getUserRepository()
    {
        return $this->entityManager->getRepository($this->userClass);
    }

    /**
     * @param array $credentials = [
     *     'email' => 'email',
     *     'phone' => 'phone',
     *     'username' => 'username'
     * ]
     * @return null
     */
    public function findUserByCredentials(array $credentials = []){
        $credentials = array_merge([
            'email' => null,
            'phone' => null,
            'username' => null
        ], $credentials);
        $userRepository = $this->getUserRepository();
        $users = [
            'phone' => null,
            'email' => null,
            'username' => null,
        ];
        if ($credentials['phone']) {
            $users['phone'] = $userRepository->findOneBy(['loginPhone' => $credentials['phone']]);
        }
        if ($credentials['phone']) {
            $users['email'] = $userRepository->findOneBy(['loginEmail' => $credentials['email']]);
        }
        if ($credentials['username']) {
            $users['username'] = $userRepository->findOneBy(['loginUsername' => $credentials['username']]);
        }
        if (!$users['phone'] && !$users['email'] && !$users['username']) {
            return null;
        }
        $currentUser = null;
        foreach($users as $key => $value){
            if($currentUser && $value !== $currentUser){
                throw new BadRequestHttpException("email_phone_mismatch");
            }
            $currentUser = $value;
        }
        /** @var MultiLoginUserInterface $user */
        return $currentUser;
    }

}