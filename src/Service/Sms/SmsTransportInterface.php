<?php


namespace Sungazer\Bundle\UserBundle\Service\Sms;


use Sungazer\Bundle\UserBundle\Model\Sms\Sms;

interface SmsTransportInterface
{

    public function send(Sms $sms);

}