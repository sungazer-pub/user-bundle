<?php


namespace Sungazer\Bundle\UserBundle\Service\Sms;


use Sungazer\Bundle\UserBundle\Model\Sms\Sms;

interface SmsSenderInterface
{

    public function send(Sms $sms);

}