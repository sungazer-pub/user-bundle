<?php


namespace Sungazer\Bundle\UserBundle\Service\Sms;


use Sungazer\Bundle\UserBundle\Model\Sms\Sms;

class SmsSender implements SmsSenderInterface
{
    /**
     * @var SmsHelper
     */
    private $helper;
    /**
     * @var SmsTransportInterface
     */
    private $smsTransport;

    public function __construct(SmsHelper $helper, SmsTransportInterface $smsTransport)
    {
        $this->helper       = $helper;
        $this->smsTransport = $smsTransport;
    }

    public function send(Sms $sms)
    {
        $sms = $this->helper->prepareSms($sms);
        $this->smsTransport->send($sms);
    }
}