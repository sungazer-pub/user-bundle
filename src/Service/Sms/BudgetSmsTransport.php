<?php


namespace Sungazer\Bundle\UserBundle\Service\Sms;


use Sungazer\Bundle\UserBundle\Model\Sms\Sms;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class BudgetSmsTransport implements SmsTransportInterface
{

    const baseUrl = "https://api.budgetsms.net";

    private $username;
    private $userid;
    private $handle;
    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    public function __construct($dsn, HttpClientInterface $httpClient)
    {
        $parsed = parse_url($dsn);
        $query  = [];
        parse_str($parsed['query'] ?? '', $query);

        $this->username   = $query['username'] ?? null;
        $this->userid     = $query['userid'] ?? null;
        $this->handle     = $query['handle'] ?? null;
        $this->httpClient = $httpClient;
    }

    public function send(Sms $sms)
    {
        // Sanitize phone number
        $sms->setTo(preg_replace("/[^\d]+/","",$sms->getTo()));
        $resp = $this->httpClient->request("GET", self::baseUrl . "/sendsms", [
            'query' => [
                'username' => $this->username,
                'userid'   => $this->userid,
                'handle'   => $this->handle,
                'from'     => $sms->getFrom(),
                'to'       => $sms->getTo(),
                'msg'      => $sms->getMessage()
            ]
        ])->getContent();
        return explode(" ", $resp);
    }

    public function testSms(Sms $sms)
    {
        // Sanitize phone number
        $sms->setTo(preg_replace("/[^\d]+/","",$sms->getTo()));
        $resp = $this->httpClient->request("GET", self::baseUrl . "/testsms", [
            'query' => [
                'username' => $this->username,
                'userid'   => $this->userid,
                'handle'   => $this->handle,
                'from'     => $sms->getFrom(),
                'to'       => $sms->getTo(),
                'msg'      => $sms->getMessage()
            ]
        ])->getContent();
        return explode(" ", $resp);
    }
}