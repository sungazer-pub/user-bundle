<?php


namespace Sungazer\Bundle\UserBundle\Service\Sms;


use Psr\Log\LoggerInterface;
use Sungazer\Bundle\UserBundle\Model\Sms\Sms;
use Sungazer\Bundle\UserBundle\Model\Sms\TemplatedSms;
use Twig\Environment;

class SmsHelper
{
    /**
     * @var string
     */
    private $smsFrom;
    /**
     * @var Environment
     */
    private $twig;

    public function __construct(Environment $twig, ?string $smsFrom)
    {
        $this->smsFrom = $smsFrom;
        $this->twig    = $twig;
    }

    /**
     * @param TemplatedSms | Sms $sms
     * @return Sms
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function prepareSms($sms): Sms
    {
        if (!$sms->getFrom()) {
            $sms->setFrom($this->smsFrom);
        }
        if ($sms instanceof TemplatedSms) {
            $template = $this->twig->load($sms->getTextTemplate());
            $sms->setMessage($template->render($sms->getContext()));
        }
        return $sms;
    }

}