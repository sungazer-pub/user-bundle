<?php


namespace Sungazer\Bundle\UserBundle\Service;


use Doctrine\ORM\EntityManagerInterface;
use Sungazer\Bundle\UserBundle\Event\UserCreatedEvent;
use Sungazer\Bundle\UserBundle\Model\MultiLoginUserInterface;
use Sungazer\Bundle\UserBundle\Model\Sms\TemplatedSms;
use Sungazer\Bundle\UserBundle\Entity\LoginToken;
use Sungazer\Bundle\UserBundle\Service\Sms\SmsSenderInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

// TODO Configure user property names
// TODO Use events for more exensibility

class AuthService
{

    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var TokenManager
     */
    private $tokenManager;
    /**
     * @var MailerInterface
     */
    private $mailer;
    /**
     * @var SmsSenderInterface
     */
    private $smsSender;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var EmailHelper
     */
    private $emailHelper;

    public function __construct(UserManager $userManager,
                                TokenManager $tokenManager,
                                TokenStorageInterface $tokenStorage,
                                MailerInterface $mailer,
                                SmsSenderInterface $smsSender,
                                EntityManagerInterface $entityManager,
                                EventDispatcherInterface $eventDispatcher,
                                UserPasswordEncoderInterface $passwordEncoder,
                                EmailHelper $emailHelper
    )
    {
        $this->userManager     = $userManager;
        $this->tokenManager    = $tokenManager;
        $this->mailer          = $mailer;
        $this->smsSender       = $smsSender;
        $this->eventDispatcher = $eventDispatcher;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager   = $entityManager;
        $this->tokenStorage    = $tokenStorage;
        $this->emailHelper = $emailHelper;
    }


    /**
     * @param array $data = [
     *     'email' => 'string',
     *     'phone' => 'string'
     * ]
     */
    public function handleRequestToken(array $data)
    {
        /** @var MultiLoginUserInterface $user */
        $user  = null;
        $email = null;
        $phone = null;
        // Detect auth scheme
        if (array_key_exists('email', $data)) {
            // TODO Check if email token is allowed
            $email = $data['email'];
            $user  = $this->userManager->getUserRepository()->findOneBy(['loginEmail' => $email]);
        } else if (array_key_exists('phone', $data)) {
            // TODO Check if phone token is allowed
            $phone = $data['phone'];
            $user  = $this->userManager->getUserRepository()->findOneBy(['loginPhone' => $phone]);
        } else {
            throw new BadRequestHttpException('Wrong combination of login parameters');
        }

        // Create user if not existing
        if (!$user) {
            $user = $this->userManager->createUser();
            if ($email) {
                $user->setLoginEmail($email);
            } else if ($phone) {
                $user->setLoginPhone($phone);
            }
            /** @var UserCreatedEvent $event */
            /** @noinspection PhpMethodParametersCountMismatchInspection */
            $event = $this->eventDispatcher->dispatch(new UserCreatedEvent($user), UserCreatedEvent::NAME);
            $user  = $event->getUser();
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        $this->tokenManager->deleteExpiredAndPreviousLoginTokensForUser($user->getId());

        // Create new token
        $token = $this->tokenManager->createLoginToken($user->getId(), 'PT1H');
        $this->entityManager->persist($token);
        if ($email) {
            $token->setDeliveryMethod(LoginToken::DELIVERY_EMAIL);
        } else if ($phone) {
            $token->setDeliveryMethod(LoginToken::DELIVERY_PHONE);
        }
        $this->entityManager->flush();

        // Send email/sms
        if ($email) {
            $this->emailHelper->loadEmailFromTemplateAndSend($user->getLoginEmail(),"@SungazerUser/emails/login_token.html.twig", [
                'user' => $user,
                'token' => $token
            ]);
//            $this->sendLoginTokenEmail($user, $token);
        } else if ($phone) {
            $this->sendLoginTokenPhone($user, $token);
        }
    }

    public function sendLoginTokenEmail(MultiLoginUserInterface $user, LoginToken $token)
    {
        $email = new TemplatedEmail();
        $email->from($_SERVER["MAILER_FROM"])
            ->to($user->getLoginEmail())
            ->subject("Your token")
            ->htmlTemplate('@SungazerUser/emails/login_token.html.twig')
            ->context([
                "user"  => $user,
                "token" => $token
            ]);
        $this->mailer->send($email);
    }

    public function sendLoginTokenPhone(MultiLoginUserInterface $user, LoginToken $token)
    {
        $sms = new TemplatedSms();
        $sms->setTo($user->getLoginPhone())
            ->setTextTemplate('@SungazerUser/sms/login_token.twig')
            ->setContext(['token' => $token]);
        $this->smsSender->send($sms);
    }

    public function authenticateUser(Request $request, MultiLoginUserInterface $user)
    {
        $token = new PostAuthenticationGuardToken($user, 'auth_service', $user->getRoles());
        $this->tokenStorage->setToken($token);
        $event = new InteractiveLoginEvent($request, $token);
        $this->eventDispatcher->dispatch($event, SecurityEvents::INTERACTIVE_LOGIN);
        return $token;
    }

}