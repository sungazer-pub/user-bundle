<?php


namespace Sungazer\Bundle\UserBundle\Service;


use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class EmailHelper
{
    /**
     * @var Environment
     */
    private $twig;
    /**
     * @var string
     */
    private $mailerFrom;
    /**
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(string $mailerFrom, Environment $twig, MailerInterface $mailer)
    {
        $this->twig       = $twig;
        $this->mailerFrom = $mailerFrom;
        $this->mailer     = $mailer;
    }

    public function loadEmailFromTemplateAndSend(string $to, string $template, array $context = [])
    {
        $email = $this->loadEmailFromTemplate($template, $context);
        $email->to($to);
        $this->mailer->send($email);
    }

    public function loadEmailFromTemplate(string $template, array $context = [])
    {
        $email    = new Email();
        $template = $this->twig->load($template);
        $email->from($this->mailerFrom)
            ->subject($template->renderBlock('subject', $context))
            ->html($template->renderBlock('body_html', $context))
            ->text($template->hasBlock('body_text') ? $template->renderBlock('body_text', $context) : null);
        return $email;
    }
}