<?php


namespace Sungazer\Bundle\UserBundle\Service;


use Doctrine\ORM\EntityManagerInterface;
use Sungazer\Bundle\UserBundle\Model\MultiLoginUserInterface;
use Sungazer\Bundle\UserBundle\Entity\EmailVerificationToken;
use Sungazer\Bundle\UserBundle\Entity\LoginToken;
use Sungazer\Bundle\UserBundle\Entity\PasswordResetToken;
use Sungazer\Bundle\UserBundle\Entity\PhoneVerificationToken;

class TokenManager
{

    /**
     * @var string
     */
    private $loginTokenAllowedChars;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var string
     */
    private $emailTokenAllowedChars;
    /**
     * @var string
     */
    private $phoneTokenAllowedChars;

    public function __construct(string $loginTokenAllowedChars,
                                string $emailTokenAllowedChars,
                                string $phoneTokenAllowedChars,
                                EntityManagerInterface $entityManager)
    {
        $this->loginTokenAllowedChars = $loginTokenAllowedChars;
        $this->entityManager          = $entityManager;
        $this->emailTokenAllowedChars = $emailTokenAllowedChars;
        $this->phoneTokenAllowedChars = $phoneTokenAllowedChars;
    }

    /**
     * @param null $userId
     * @param string $intervalSpec
     * @return LoginToken
     */
    public function createLoginToken($userId = null, string $intervalSpec = "PT1H")
    {
        $token = new LoginToken($userId, $intervalSpec);
        $token->setToken($this->createTokenString(6, $this->loginTokenAllowedChars));
        return $token;
    }

    public function createLoginEmailVerificationToken(string $userId, string $email, string $intervalSpec = "PT1H")
    {
        $token = new EmailVerificationToken($userId, $intervalSpec);
        $token
            ->setEmail($email)
            ->setToken($this->createTokenString(6, $this->emailTokenAllowedChars));
        return $token;
    }

    public function createLoginPhoneVerificationToken(string $userId, string $phone, string $intervalSpec = "PT1H")
    {
        $token = new PhoneVerificationToken($userId, $intervalSpec);
        $token
            ->setPhone($phone)
            ->setToken($this->createTokenString(6, $this->emailTokenAllowedChars));
        return $token;
    }

    public function createPasswordResetToken(string $userId, string $intervalSpec = "PT1H")
    {
        $token = new PasswordResetToken($userId, $intervalSpec);
        $token
            ->setToken($this->createTokenString(6, $this->emailTokenAllowedChars));

        // Verify that token does not exist in db
        $count = 0;
        while($this->passwordResetTokenExistsInDb($token->getToken())){
            $token
                ->setToken($this->createTokenString(6, $this->emailTokenAllowedChars));
            $count++;
            if($count === 50){
                throw new \Exception('cannot_generate_password_reset_token');
            }
        }

        return $token;
    }

    private function passwordResetTokenExistsInDb(string $token){
        $result = $this->entityManager->createQueryBuilder()
            ->select('p')
            ->from(PasswordResetToken::class,'p')
            ->where('p.id = :token')
            ->setParameter('token',$token)
            ->getQuery()->getResult();
        return !!$result;
    }

    /**
     * @param int $length
     * @param string $allowedChars
     * @return string
     */
    public function createTokenString(int $length, string $allowedChars = "0123456789")
    {
        $len    = strlen($allowedChars);
        $result = "";
        for ($i = 0; $i < $length; $i++) {
            $result .= $allowedChars[mt_rand(0, $len - 1)];
        }
        return $result;
    }

    public function deleteExpiredAndPreviousLoginTokensForUser(string $userId)
    {
        $this->deleteExpiredAndPreviousTokensForUser($userId,LoginToken::class);
    }

    public function deleteExpiredAndPreviousTokensForUser(string $userId, string $tokenClass)
    {
        $this->entityManager->createQueryBuilder()
            ->delete($tokenClass, "tok")
            ->where("tok.userId = :userId")
            ->orWhere("tok.expiresAt <= CURRENT_TIMESTAMP()")
            ->setParameter('userId', $userId)
            ->getQuery()->execute();
    }

}