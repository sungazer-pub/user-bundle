<?php


namespace Sungazer\Bundle\UserBundle\Security;

use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationFailureHandler;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Sungazer\Bundle\UserBundle\Event\UserLoginEvent;
use Sungazer\Bundle\UserBundle\Lib\DtoSerializer;
use Sungazer\Bundle\UserBundle\Service\UserManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use UnexpectedValueException;

class JsonMultiLoginAuthenticator extends AbstractGuardAuthenticator
{
    private $em;
    /**
     * @var DtoSerializer
     */
    private $dtoSerializer;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var AuthenticationSuccessHandler
     */
    private $authenticationSuccessHandler;
    /**
     * @var AuthenticationFailureHandler
     */
    private $authenticationFailureHandler;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(EntityManagerInterface       $em,
                                DtoSerializer                $dtoSerializer,
                                UserPasswordEncoderInterface $passwordEncoder,
                                UserManager                  $userManager,
                                AuthenticationSuccessHandler $authenticationSuccessHandler,
                                AuthenticationFailureHandler $authenticationFailureHandler,
                                EventDispatcherInterface     $eventDispatcher
    )
    {
        $this->em                           = $em;
        $this->dtoSerializer                = $dtoSerializer;
        $this->passwordEncoder              = $passwordEncoder;
        $this->authenticationSuccessHandler = $authenticationSuccessHandler;
        $this->authenticationFailureHandler = $authenticationFailureHandler;
        $this->userManager                  = $userManager;
        $this->eventDispatcher              = $eventDispatcher;
    }

    /**
     * Returns a response that directs the user to authenticate.
     *
     * This is called when an anonymous request accesses a resource that
     * requires authentication. The job of this method is to return some
     * response that "helps" the user start into the authentication process.
     *
     * Examples:
     *
     * - For a form login, you might redirect to the login page
     *
     *     return new RedirectResponse('/login');
     *
     * - For an API token authentication system, you return a 401 response
     *
     *     return new Response('Auth header required', 401);
     *
     * @param Request $request The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     *
     * @return Response
     */
    public
    function start(Request $request, AuthenticationException $authException = null)
    {
        return new Response('Auth header required', 401);
    }

    /**
     * Does the authenticator support the given Request?
     *
     * If this returns false, the authenticator will be skipped.
     *
     * @param Request $request
     * @return bool
     */
    public
    function supports(Request $request)
    {
        return true;
    }

    /**
     * Get the authentication credentials from the request and return them
     * as any type (e.g. an associate array).
     *
     * Whatever value you return here will be passed to getUser() and checkCredentials()
     *
     * For example, for a form login, you might:
     *
     *      return [
     *          'username' => $request->request->get('_username'),
     *          'password' => $request->request->get('_password'),
     *      ];
     *
     * Or for an API token that's on a header, you might use:
     *
     *      return ['api_key' => $request->headers->get('X-API-TOKEN')];
     *
     * @return mixed Any non-null value
     *
     * @throws UnexpectedValueException If null is returned
     */
    public
    function getCredentials(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        return $data;
    }

    /**
     * Return a UserInterface object based on the credentials.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * You may throw an AuthenticationException if you wish. If you return
     * null, then a UsernameNotFoundException is thrown for you.
     *
     * @param array $credentials = [
     *     'username' => 'string',
     *     'email' => 'string',
     *     'phone' => 'string'
     *     'password' => 'string',
     *     'token' => 'string'
     * ]
     *
     * @return UserInterface|null
     * @throws AuthenticationException
     *
     */
    public
    function getUser($credentials, UserProviderInterface $userProvider)
    {
        $credentials = array_merge([
            'username' => null,
            'email'    => null,
            'phone'    => null,
            'password' => null,
            'token'    => null,
        ], $credentials);

        /** @var UserLoginEvent $result */
        $result = $this->eventDispatcher->dispatch(new UserLoginEvent($credentials), UserLoginEvent::NAME);

        if (!$result->getCredentialsvalidated()) {
            throw new AuthenticationException('Missing auth secret (i.e. password or token)');
        }

        return $result->getUser();
    }

    /**
     * Returns true if the credentials are valid.
     *
     * If any value other than true is returned, authentication will
     * fail. You may also throw an AuthenticationException if you wish
     * to cause authentication to fail.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * @param mixed $credentials
     *
     * @return bool
     *
     * @throws AuthenticationException
     */
    public
    function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the login page or a 403 response.
     *
     * If you return null, the request will continue, but the user will
     * not be authenticated. This is probably not what you want to do.
     *
     * @return Response|null
     */
    public
    function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return $this->authenticationFailureHandler->onAuthenticationFailure($request, $exception);
    }

    /**
     * Called when authentication executed and was successful!
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the last page they visited.
     *
     * If you return null, the current request will continue, and the user
     * will be authenticated. This makes sense, for example, with an API.
     *
     * @param string $providerKey The provider (i.e. firewall) key
     *
     * @return Response|null
     */
    public
    function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return $this->authenticationSuccessHandler->onAuthenticationSuccess($request, $token);
    }

    /**
     * Does this method support remember me cookies?
     *
     * Remember me cookie will be set if *all* of the following are met:
     *  A) This method returns true
     *  B) The remember_me key under your firewall is configured
     *  C) The "remember me" functionality is activated. This is usually
     *      done by having a _remember_me checkbox in your form, but
     *      can be configured by the "always_remember_me" and "remember_me_parameter"
     *      parameters under the "remember_me" firewall key
     *  D) The onAuthenticationSuccess method returns a Response object
     *
     * @return bool
     */
    public
    function supportsRememberMe()
    {
        return false;
    }
}
