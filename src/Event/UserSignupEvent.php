<?php


namespace Sungazer\Bundle\UserBundle\Event;


use Sungazer\Bundle\UserBundle\Model\MultiLoginUserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\Event;

class UserSignupEvent extends Event
{
    public const NAME = 'sungazer_user.user_signup';
    /**
     * @var MultiLoginUserInterface
     */
    private $user;

    /**
     * Request data
     * @var array
     */
    private $data;

    /**
     * Original http request
     * @var Request
     */
    private $request;

    public function __construct(Request $request, array $data)
    {
        $this->request = $request;
        $this->data    = $data;
    }

    /**
     * @return array = [
     *     'email' => 'string',
     *     'phone' => 'string',
     *     'username' => 'string',
     *     'password' => 'string',
     *     'password2' => 'string',
     *     'verifyEmailUrl' => 'url',
     *     'verifyPhoneUrl' => 'url'
     * ]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return UserSignupEvent
     */
    public function setData(array $data): UserSignupEvent
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @param Request $request
     * @return UserSignupEvent
     */
    public function setRequest(Request $request): UserSignupEvent
    {
        $this->request = $request;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(MultiLoginUserInterface $user)
    {
        $this->user = $user;
        return $this;
    }
}