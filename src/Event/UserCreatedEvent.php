<?php


namespace Sungazer\Bundle\UserBundle\Event;


use Sungazer\Bundle\UserBundle\Model\MultiLoginUserInterface;
use Symfony\Contracts\EventDispatcher\Event;

class UserCreatedEvent extends Event
{
    public const NAME = 'sungazer_user.user_created';
    /**
     * @var MultiLoginUserInterface
     */
    private $user;

    public function __construct(MultiLoginUserInterface $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(MultiLoginUserInterface $user)
    {
        $this->user = $user;
        return $this;
    }
}