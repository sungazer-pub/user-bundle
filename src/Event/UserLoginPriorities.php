<?php


namespace Sungazer\Bundle\UserBundle\Event;


class UserLoginPriorities
{
    const PRE_RETRIEVE_USER = self::RETRIEVE_USER + 1;
    const RETRIEVE_USER = 40;
    const POST_RETRIEVE_USER = self::RETRIEVE_USER - 1;

    const PRE_VERIFY_USER = self::VERIFY_USER + 1;
    const VERIFY_USER = 32;
    const POST_VERIFY_USER = self::VERIFY_USER - 1;

    const PRE_VALIDATE_CREDENTIALS = self::VALIDATE_CREDENTIALS + 1;
    const VALIDATE_CREDENTIALS = 24;
    const POST_VALIDATE_CREDENTIALS = self::VALIDATE_CREDENTIALS - 1;
}