<?php


namespace Sungazer\Bundle\UserBundle\Event;


use Sungazer\Bundle\UserBundle\Model\MultiLoginUserInterface;
use Symfony\Contracts\EventDispatcher\Event;

class UserLoginEvent extends Event
{
    public const NAME = 'sungazer_user.user_login';
    /**
     * @var MultiLoginUserInterface | null
     */
    private $user;
    /**
     * @var array
     */
    private $credentials;

    private $credentialsValidated = false;

    public function __construct(array $credentials)
    {
        $this->credentials = $credentials;
    }

    /**
     * @return array
     */
    public function getCredentials(): array
    {
        return $this->credentials;
    }

    /**
     * @param array $credentials
     * @return UserLoginEvent
     */
    public function setCredentials(array $credentials): UserLoginEvent
    {
        $this->credentials = $credentials;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(?MultiLoginUserInterface $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return bool
     */
    public function getCredentialsValidated(): bool
    {
        return $this->credentialsValidated;
    }

    /**
     * @param bool $credentialsValidated
     */
    public function setCredentialsValidated(bool $credentialsValidated): void
    {
        $this->credentialsValidated = $credentialsValidated;
    }
}