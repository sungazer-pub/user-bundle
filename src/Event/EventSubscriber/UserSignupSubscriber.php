<?php


namespace Sungazer\Bundle\UserBundle\Event\EventSubscriber;


use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sungazer\Bundle\UserBundle\Entity\EmailVerificationToken;
use Sungazer\Bundle\UserBundle\Entity\PhoneVerificationToken;
use Sungazer\Bundle\UserBundle\Event\UserCreatedPriorities;
use Sungazer\Bundle\UserBundle\Event\UserSignupEvent;
use Sungazer\Bundle\UserBundle\Model\MultiLoginUserInterface;
use Sungazer\Bundle\UserBundle\Model\Sms\TemplatedSms;
use Sungazer\Bundle\UserBundle\Service\EmailHelper;
use Sungazer\Bundle\UserBundle\Service\Sms\SmsSenderInterface;
use Sungazer\Bundle\UserBundle\Service\TokenManager;
use Sungazer\Bundle\UserBundle\Service\UserManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserSignupSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TokenManager
     */
    private $tokenManager;
    /**
     * @var EmailHelper
     */
    private $emailHelper;
    /**
     * @var SmsSenderInterface
     */
    private $smsSender;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(UserManager $userManager,
                                UserPasswordEncoderInterface $passwordEncoder,
                                EntityManagerInterface $entityManager,
                                TokenManager $tokenManager,
                                EmailHelper $emailHelper,
                                LoggerInterface $logger,
                                SmsSenderInterface $smsSender)
    {
        $this->userManager     = $userManager;
        $this->entityManager   = $entityManager;
        $this->tokenManager    = $tokenManager;
        $this->emailHelper     = $emailHelper;
        $this->smsSender       = $smsSender;
        $this->passwordEncoder = $passwordEncoder;
        $this->logger          = $logger;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            UserSignupEvent::NAME => [
                ['createUser', UserCreatedPriorities::CREATE_USER],
                ['persistUser', UserCreatedPriorities::PERSIST_USER],
                ['sendSignupVerificationCode', UserCreatedPriorities::SEND_NOTIFICATION]
            ]
        ];
    }

    public function createUser(UserSignupEvent $event)
    {
        $userRepository = $this->userManager->getUserRepository();
        $data           = $event->getData();
        $email          = $data['email'] ?? null;
        $username       = $data['username'] ?? null;
        $phone          = $data['phone'] ?? null;

        $userByUsername = $username ? $userRepository->findOneBy(['loginUsername' => $username]) : null;
        $userByEmail    = $email ? $userRepository->findOneBy(['loginEmail' => $email]) : null;
        $userByPhone    = $phone ? $userRepository->findOneBy(['loginPhone' => $phone]) : null;

        if ($userByUsername) {
            throw new BadRequestHttpException("username_taken");
        } else if ($userByEmail) {
            throw new BadRequestHttpException("email_taken");
        } else if ($userByPhone) {
            throw new BadRequestHttpException("phone_taken");
        }

        // Create user
        /** @var MultiLoginUserInterface $user */
        $user = $event->getUser() ?? $this->userManager->createUser();
        if ($email) {
            $user->setLoginEmail($email);
        }
        if ($phone) {
            $user->setLoginPhone($phone);
        }

        // Set password
        $password  = $data['password'] ?? null;
        $password2 = $data['password2'] ?? null;
        if (!$password || !$password2) {
            throw new BadRequestHttpException("missing_password");
        }
        if ($password !== $password2) {
            throw new BadRequestHttpException("password_mismatch");
        }
        $user->setPassword($this->passwordEncoder->encodePassword($user, $password));

        $event->setUser($user);
        $this->logger->debug("Created user", ['user' => $user->getId(), 'roles' => $user->getRoles()]);
    }

    public function persistUser(UserSignupEvent $event)
    {
        $user = $event->getUser();
        if (!$this->entityManager->contains($user)) {
            $this->entityManager->persist($user);
        }
        $this->entityManager->flush();
        $this->logger->debug("Persisted user", ['user' => $user->getId(), 'roles' => $user->getRoles()]);
    }

    public function sendSignupVerificationCode(UserSignupEvent $event)
    {
        $user = $event->getUser();
        $data = $event->getData();
        if ($email = $user->getLoginEmail()) {
            $this->tokenManager->deleteExpiredAndPreviousTokensForUser($user->getId(), EmailVerificationToken::class);
            $token = $this->tokenManager->createLoginEmailVerificationToken($user->getId(), $user->getLoginEmail());
            $this->entityManager->persist($token);
            $this->entityManager->flush();
            $verifyEmailUrl = $data['verifyEmailUrl'] ?? null;
            $context        = [
                "user"  => $user,
                "token" => $token
            ];
            if ($verifyEmailUrl) {
                $context['verifyUrl'] = $verifyEmailUrl . "?" . http_build_query(['token' => $token->getToken()]);
            }
            $this->emailHelper->loadEmailFromTemplateAndSend($email, '@SungazerUser/emails/signup_customer.html.twig', $context);
            $this->logger->debug("Sent verification email");
        }
        if ($phone = $user->getLoginPhone()) {
            $this->tokenManager->deleteExpiredAndPreviousTokensForUser($user->getId(), PhoneVerificationToken::class);
            $token = $this->tokenManager->createLoginPhoneVerificationToken($user->getId(), $user->getLoginPhone());
            $this->entityManager->persist($token);
            $this->entityManager->flush();
            $verifyPhoneUrl = $data['verifyPhoneUrl'] ?? null;
            $context        = [
                "user"  => $user,
                "token" => $token
            ];
            if ($verifyPhoneUrl) {
                $context['verifyUrl'] = $verifyPhoneUrl . "?" . http_build_query(['token' => $token->getToken()]);
            }
            $sms = new TemplatedSms();
            $sms->setTo($phone)
                ->setTextTemplate('@SungazerUser/sms/signup_customer.twig')
                ->setContext($context);
            $this->smsSender->send($sms);
            $this->logger->debug("Sent verification sms");
        }
    }
}