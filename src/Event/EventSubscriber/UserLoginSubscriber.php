<?php


namespace Sungazer\Bundle\UserBundle\Event\EventSubscriber;


use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sungazer\Bundle\UserBundle\Entity\LoginToken;
use Sungazer\Bundle\UserBundle\Event\UserLoginEvent;
use Sungazer\Bundle\UserBundle\Event\UserLoginPriorities;
use Sungazer\Bundle\UserBundle\Model\ActivableUserInterface;
use Sungazer\Bundle\UserBundle\Model\EmailPasswordLoginableInterface;
use Sungazer\Bundle\UserBundle\Model\MultiLoginUserInterface;
use Sungazer\Bundle\UserBundle\Service\TokenManager;
use Sungazer\Bundle\UserBundle\Service\UserManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class UserLoginSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TokenManager
     */
    private $tokenManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(UserManager $userManager,
                                UserPasswordEncoderInterface $passwordEncoder,
                                EntityManagerInterface $entityManager,
                                TokenManager $tokenManager,
                                LoggerInterface $logger)
    {
        $this->userManager     = $userManager;
        $this->entityManager   = $entityManager;
        $this->tokenManager    = $tokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->logger          = $logger;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            UserLoginEvent::NAME => [
                ['retrieveUser', UserLoginPriorities::RETRIEVE_USER],
                ['verifyUser', UserLoginPriorities::VERIFY_USER],
                ['validateCredentials', UserLoginPriorities::VALIDATE_CREDENTIALS]
            ]
        ];
    }

    public function retrieveUser(UserLoginEvent $event)
    {
        $userRepository = $this->userManager->getUserRepository();
        $credentials    = $event->getCredentials();
        /** @var MultiLoginUserInterface $user */
        $user = null;
        // Detect auth scheme
        if ($credentials['username'] ?? null) {
            /** @var MultiLoginUserInterface $user */
            $user = $userRepository->findOneBy(['loginUsername' => $credentials['username']]);
        } else if ($credentials['email'] ?? null) {
            /** @var MultiLoginUserInterface $user */
            $user = $userRepository->findOneBy(['loginEmail' => $credentials['email']]);
        } else if ($credentials['phone'] ?? null) {
            /** @var MultiLoginUserInterface $user */
            $user = $userRepository->findOneBy(['loginPhone' => $credentials['phone']]);
        }

        $event->setUser($user);

        return $event;
    }

    public function validateCredentials(UserLoginEvent $event)
    {
        if ($event->getCredentialsValidated()) {
            return $event;
        }
        $credentials = $event->getCredentials();
        $user        = $event->getUser();

        $ensureEmailValidated = function() use ($user,$credentials,$event) {
            if(!$user->isLoginEmailValidated()){
                throw new AuthenticationException('email_not_validated');
            }
        };

        $ensurePasswordValid = function() use ($user,$credentials,$event) {
            if (!$this->passwordEncoder->isPasswordValid($user, $credentials['password'])) {
                throw new AuthenticationException('wrong_password');
            }
        };

        // New behavior based implementation
        if(is_subclass_of($user,EmailPasswordLoginableInterface::class)){
            if (($credentials['email'] ?? null) && ($credentials['password'] ?? null)) {
                $ensureEmailValidated();
                $ensurePasswordValid();
                $event->setCredentialsValidated(true);
                return $event;
            }
        }
        // Fallback
        if (is_subclass_of($user, MultiLoginUserInterface::class)){
            if (($credentials['email'] ?? null) && ($credentials['password'] ?? null)) {
                $ensureEmailValidated();
                $ensurePasswordValid();
                $event->setCredentialsValidated(true);
                return $event;
            } else if ($credentials['token'] ?? null) {
                $tokenRepository = $this->entityManager->getRepository(LoginToken::class);
                $token           = $tokenRepository->findOneBy(["id" => $credentials['token']]);
                if (!$token) {
                    throw new AuthenticationException('wrong_token');
                } else if ($token->isExpired()) {
                    throw new AuthenticationException('expired_token');
                } else {
                    if ($token->getUserId() !== $user->getId()) {
                        throw new AuthenticationException('token_user_mismatch');
                    }
                    $this->entityManager->remove($token);
                    $this->entityManager->flush();
                    // Mark as validated the source of the token (i.e. email or phone) for the user
                    switch ($token->getDeliveryMethod()) {
                        case LoginToken::DELIVERY_PHONE:
                            $user->setLoginPhoneValidated(true);
                            break;
                        case LoginToken::DELIVERY_EMAIL:
                            $user->setLoginEmailValidated(true);
                            break;
                    }
                    $event->setCredentialsValidated(true);
                    return $event;
                }
            }
        }

        return $event;
    }

    public function verifyUser(UserLoginEvent $event)
    {
        $user = $event->getUser();
        if (!$user) {
            throw new AuthenticationException('user_not_found');
        }

        // Handle ActivableUser
        if ($user instanceof ActivableUserInterface) {
            if ($user->getActive() === false) {
                throw new AuthenticationException('user_disabled');
            }
        }

        return $event;
    }

}