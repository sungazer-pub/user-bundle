<?php


namespace Sungazer\Bundle\UserBundle\Event;


class UserCreatedPriorities
{
    const PRE_CREATE_USER = self::CREATE_USER + 1;
    const CREATE_USER = 32;
    const POST_CREATE_USER = self::CREATE_USER - 1;

    const PRE_PERSIST_USER = self::PERSIST_USER + 1;
    const PERSIST_USER = 24;
    const POST_PERSIST_USER = self::PERSIST_USER - 1;

    const PRE_SEND_NOTIFICATION = self::SEND_NOTIFICATION + 1;
    const SEND_NOTIFICATION = 16;
    const POST_SEND_NOTIFICATION = self::SEND_NOTIFICATION - 1;
}