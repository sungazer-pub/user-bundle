<?php


namespace Sungazer\Bundle\UserBundle\Lib;


use Sungazer\Bundle\UserBundle\Exception\ValidatorViolationHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DtoSerializer
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator)
    {

        $this->serializer = $serializer;
        $this->validator  = $validator;
    }

    /**
     * @param Request $request
     * @param string $dtoClass
     * @param string $format
     * @return object of class $dtoClass
     * @throws HttpException
     */
    public function deserializeBody(Request $request, string $dtoClass, string $format = 'json')
    {
        $res = $this->serializer->deserialize($request->getContent(), $dtoClass, $format);
        // TODO Proper json serialization
        $violations = $this->validator->validate($res);
        if ($violations->count() > 0) {
            throw new ValidatorViolationHttpException($violations);
        }

        return $res;
    }

    public function serialize($object, string $format = 'json')
    {
        $res = $this->serializer->serialize($object, $format);
        // TODO Validation
        return $res;
    }
}