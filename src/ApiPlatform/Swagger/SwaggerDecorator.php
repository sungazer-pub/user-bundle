<?php


namespace Sungazer\Bundle\UserBundle\ApiPlatform\Swagger;


use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class SwaggerDecorator implements NormalizerInterface
{

    private $decorated;

    public function __construct(NormalizerInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $docs = $this->decorated->normalize($object, $format, $context);
        $this->fixDefinitions($docs);

        return $docs;
    }

    private function fixDefinitions(array &$docs)
    {
        $paths = $docs["paths"];
//        dump($paths);
        $paths["/api/auth/login"]          = [
            "post" => [
                "tags"        => ["Authentication"],
                "operationId" => "login",
                "produces"    => ["application/json"],
                "summary"     => "Login",
                "description" => "Login",
                "responses"   => [
                    "200" => [],
                    "401" => []
                ],
                "requestBody" => $this->createRequestBody([
                    'required' => true,
                    'content'  => [
                        'application/json' => [
                            'schema' => [
                                'type'       => 'object',
                                'properties' => [
                                    "username" => ["type" => "string"],
                                    "email"    => ["type" => "string"],
                                    "password" => ["type" => "string"],
                                    "phone"    => ["type" => "string"],
                                    "token"    => ["type" => "string"],
                                ]
                            ]
                        ]
                    ]
                ])
            ]
        ];
        $paths["/api/auth/request_token"]  = [
            "post" => [
                "tags"        => ["Authentication"],
                "operationId" => "request_token",
                "produces"    => ["application/json"],
                "summary"     => "Request login token",
                "description" => "Request login token",
                "responses"   => [
                    "200" => [],
                    "400" => []
                ],
                "requestBody" => $this->createRequestBody([
                    'required' => true,
                    'content'  => [
                        'application/json' => [
                            'schema' => $this->createObject([
                                "email" => ["type" => "string"],
                                "phone" => ["type" => "string"]
                            ])
                        ]
                    ]
                ])
            ]
        ];
        $paths["/api/auth/signup"]         = [
            "post" => [
                "tags"        => ["Authentication"],
                "operationId" => "signup",
                "produces"    => ["application/json"],
                "summary"     => "Signup",
                "description" => "Signup",
                "responses"   => [
                    "200" => [],
                    "400" => []
                ],
                "requestBody" => $this->createRequestBody([
                    'required' => true,
                    'content'  => [
                        'application/json' => [
                            'schema' => $this->createObject([
                                "username"  => ["type" => "string"],
                                "email"     => ["type" => "string"],
                                "phone"     => ["type" => "string"],
                                "password"  => ["type" => "string"],
                                "password2" => ["type" => "string"],
                            ])
                        ]
                    ]
                ])
            ]
        ];
        $paths["/api/auth/verify_phone"]   = [
            "post" => [
                "tags"        => ["Authentication"],
                "operationId" => "verify_phone",
                "produces"    => ["application/json"],
                "summary"     => "Verify phone number",
                "description" => "Verify phone number",
                "responses"   => [
                    "200" => [],
                    "400" => []
                ],
                "requestBody" => $this->createRequestBody([
                    'required' => true,
                    'content'  => [
                        'application/json' => [
                            'schema' => $this->createObject([
                                "token"     => ["type" => "string"],
                                "password"  => ["type" => "string"],
                                "password2" => ["type" => "string"],
                            ])
                        ]
                    ]
                ])
            ]
        ];
        $paths["/api/auth/verify_email"]   = [
            "post" => [
                "tags"        => ["Authentication"],
                "operationId" => "verify_email",
                "produces"    => ["application/json"],
                "summary"     => "Verify email",
                "description" => "Verify email",
                "responses"   => [
                    "200" => [],
                    "400" => []
                ],
                "requestBody" => $this->createRequestBody([
                    'required' => true,
                    'content'  => [
                        'application/json' => [
                            'schema' => $this->createObject([
                                "token"     => ["type" => "string"],
                                "password"  => ["type" => "string"],
                                "password2" => ["type" => "string"],
                            ])
                        ]
                    ]
                ])
            ]
        ];
        $paths["/api/auth/reset_password"] = [
            "post" => [
                "tags"        => ["Authentication"],
                "operationId" => "reset_password",
                "produces"    => ["application/json"],
                "summary"     => "Reset password",
                "description" => "Reset password",
                "responses"   => [
                    "200" => [],
                    "400" => []
                ],
                "requestBody" => $this->createRequestBody([
                    'required' => true,
                    'content'  => [
                        'application/json' => [
                            'schema' => $this->createObject([
                                "email"         => ["type" => "string"],
                                "phone"         => ["type" => "string"],
                                "emailResetUrl" => ["type" => "string"],
                                "phoneResetUrl" => ["type" => "string"],
                                "token"         => ["type" => "string"],
                                "password"      => ["type" => "string"],
                                "password2"     => ["type" => "string"],
                            ])
                        ]
                    ]
                ])
            ]
        ];

    }

    /**
     * @param array $data = [
     *     'description' => 'markdown',
     *     'required' => 'boolean',
     *     'content' => 'array'
     * ]
     * @return array
     */
    private function createRequestBody(array $data)
    {
        return $data;
    }

    private function createObject(array $properties)
    {
        return [
            'type'       => 'object',
            'properties' => $properties
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

}