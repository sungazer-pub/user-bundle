<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 9.14
 */

namespace Sungazer\Bundle\UserBundle\DependencyInjection;


use Exception;
use InvalidArgumentException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class SungazerUserExtension extends Extension
{

    public function getAlias()
    {
        return 'sungazer_user';
    }

    /**
     * Loads a specific configuration.
     *
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();

        /** @var array $config = [
         *    "user_class" => "string",
         *    "login_token" => [
         *      "allowed_chars" => "string",
         *    ],
         *    "email_token" => [
         *      "allowed_chars" => "string",
         *    ],
         *    "phone_token" => [
         *      "allowed_chars" => "string",
         *    ]
         *    "mailer_from" => "string",
         *    "sms" => [
         *      "from" => "string",
         *      "dsn" => "dsn"
         *    ]
         * ]
         */
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load("services.xml");

        $container->setParameter("sungazer_user.user_class", $config["user_class"]);

        $container->setParameter('sungazer_user.login_token.allowed_chars', $config['login_token']['allowed_chars']);
        $container->setParameter('sungazer_user.email_token.allowed_chars', $config['email_token']['allowed_chars']);
        $container->setParameter('sungazer_user.phone_token.allowed_chars', $config['phone_token']['allowed_chars']);

        $container->setParameter('sungazer_user.mailer_from', $config['mailer_from']);

        $container->setParameter('sungazer_user.sms.from', $config['sms']['from']);
        $container->setParameter('sungazer_user.sms.dsn', $config['sms']['dsn']);

        $bundles = $container->getParameter('kernel.bundles');

        if (isset($bundles['ApiPlatformBundle'])) {
            $loader->load('api_platform/services_api_platform.xml');
        }


    }
}