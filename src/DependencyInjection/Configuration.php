<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 11.49
 */

namespace Sungazer\Bundle\UserBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

// https://symfony.com/doc/current/bundles/configuration.html

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('sungazer_user');

        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
            ->scalarNode('user_class')
            ->defaultValue("App\Entity\AppUser")
            ->end()

            ->arrayNode('login_token')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('allowed_chars')->defaultValue('0123456789abcdefghijklmnopqrstuvwxyz')
            ->end()->end()->end()

            ->arrayNode('email_token')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('allowed_chars')->defaultValue('0123456789')
            ->end()->end()->end()

            ->arrayNode('phone_token')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('allowed_chars')->defaultValue('0123456789')
            ->end()->end()->end()

            ->scalarNode('mailer_from')
            ->defaultValue('%env(MAILER_FROM)%')
            ->end()

            ->arrayNode('sms')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('from')
            ->defaultValue('%env(default::SMS_FROM)%')
            ->end()
            ->scalarNode('dsn')
            ->defaultValue('%env(default::SMS_DSN)%')
            ->end()->end()->end()
        ;

        return $treeBuilder;
    }
}