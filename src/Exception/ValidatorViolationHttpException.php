<?php


namespace Sungazer\Bundle\UserBundle\Exception;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

class ValidatorViolationHttpException extends BadRequestHttpException
{
    /**
     * @var ConstraintViolationListInterface
     */
    public $violations;

    public function __construct(ConstraintViolationListInterface $violations, Throwable $previous = null, int $code = 0, array $headers = [])
    {
        $message = '';
        /** @var ConstraintViolationInterface $violation */
        foreach ($violations as $violation) {
            $message .= $violation->getPropertyPath() . ": " . $violation->getMessage();
        }
        parent::__construct($message, $previous, $code, $headers);
        $this->violations = $violations;
    }
}