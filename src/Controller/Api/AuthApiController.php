<?php

namespace Sungazer\Bundle\UserBundle\Controller\Api;

use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationSuccessResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Psr\Log\LoggerInterface;
use Sungazer\Bundle\UserBundle\Dto\ResetPasswordReqDto;
use Sungazer\Bundle\UserBundle\Dto\VerificationReqDto;
use Sungazer\Bundle\UserBundle\Event\UserSignupEvent;
use Sungazer\Bundle\UserBundle\Lib\DtoSerializer;
use Sungazer\Bundle\UserBundle\Model\MultiLoginUserInterface;
use Sungazer\Bundle\UserBundle\Model\Sms\TemplatedSms;
use Sungazer\Bundle\UserBundle\Entity\EmailVerificationToken;
use Sungazer\Bundle\UserBundle\Entity\PasswordResetToken;
use Sungazer\Bundle\UserBundle\Entity\PhoneVerificationToken;
use Sungazer\Bundle\UserBundle\Service\AuthService;
use Sungazer\Bundle\UserBundle\Service\EmailHelper;
use Sungazer\Bundle\UserBundle\Service\Sms\SmsSenderInterface;
use Sungazer\Bundle\UserBundle\Service\TokenManager;
use Sungazer\Bundle\UserBundle\Service\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AuthController
 */
class AuthApiController extends AbstractController
{

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var AuthService
     */
    private $authService;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var DtoSerializer
     */
    private $dtoSerializer;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;
    /**
     * @var AuthenticationSuccessHandler
     */
    private $successHandler;
    /**
     * @var TokenManager
     */
    private $tokenManager;
    /**
     * @var EmailHelper
     */
    private $emailHelper;
    /**
     * @var SmsSenderInterface
     */
    private $smsSender;

    /**
     * AuthApiController constructor.
     * @param LoggerInterface $logger
     * @param AuthService $authService
     * @param EventDispatcherInterface $eventDispatcher
     * @param DtoSerializer $dtoSerializer
     * @param EntityManagerInterface $entityManager
     * @param UserManager $userManager
     * @param TokenManager $tokenManager
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @param AuthenticationSuccessHandler $successHandler
     * @param EmailHelper $emailHelper
     * @param SmsSenderInterface $smsSender
     */
    public function __construct(LoggerInterface $logger,
                                AuthService $authService,
                                EventDispatcherInterface $eventDispatcher,
                                DtoSerializer $dtoSerializer,
                                EntityManagerInterface $entityManager,
                                UserManager $userManager,
                                TokenManager $tokenManager,
                                UserPasswordEncoderInterface $userPasswordEncoder,
                                AuthenticationSuccessHandler $successHandler,
                                EmailHelper $emailHelper,
                                SmsSenderInterface $smsSender)
    {
        $this->logger              = $logger;
        $this->authService         = $authService;
        $this->eventDispatcher     = $eventDispatcher;
        $this->dtoSerializer       = $dtoSerializer;
        $this->entityManager       = $entityManager;
        $this->userManager         = $userManager;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->successHandler      = $successHandler;
        $this->tokenManager        = $tokenManager;
        $this->emailHelper         = $emailHelper;
        $this->smsSender           = $smsSender;
    }

    public function requestToken(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $this->authService->handleRequestToken($data);

        return new Response();
    }

    /**
     * @param RequestStack $requestStack
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function signup(Request $request)
    {
//        $request = $requestStack->getMasterRequest();
        $data    = json_decode($request->getContent(), true);
        $result = $this->eventDispatcher->dispatch(new UserSignupEvent($request, $data), UserSignupEvent::NAME);
        return new Response();
    }

    /**
     * @param Request $request
     * @return JWTAuthenticationSuccessResponse
     */
    public function verifyPhone(Request $request)
    {
        /** @var VerificationReqDto $data */
        $data       = $this->dtoSerializer->deserializeBody($request, VerificationReqDto::class);
        $repository = $this->entityManager->getRepository(PhoneVerificationToken::class);

        $token = $repository->findOneBy(['id' => $data->getToken()]);
        if (!$token) {
            throw new BadRequestHttpException('no_token');
        }
        if ($token->isExpired()) {
            throw new BadRequestHttpException('expired_token');
        }


        /** @var MultiLoginUserInterface $user */
        $user = $this->userManager->getUserRepository()->findOneBy(['id' => $token->getUserId()]);
        if (!$user) {
            throw new BadRequestHttpException('no_user');
        }

        $user
            ->setLoginPhone($token->getPhone())
            ->setLoginPhoneValidated(true);

        if ($data->getPassword() && $data->getPassword() === $data->getPassword2()) {
            $user->setPassword($this->userPasswordEncoder->encodePassword($user, $data->getPassword()));
        }

        $this->entityManager->remove($token);
        $this->entityManager->flush();

        return $this->successHandler->handleAuthenticationSuccess($user);
    }

    /**
     * @param Request $request
     * @return JWTAuthenticationSuccessResponse
     */
    public function verifyEmail(Request $request)
    {
        /** @var VerificationReqDto $data */
        $data       = $this->dtoSerializer->deserializeBody($request, VerificationReqDto::class);
        $repository = $this->entityManager->getRepository(EmailVerificationToken::class);

        $token = $repository->findOneBy(['id' => $data->getToken()]);
        if (!$token) {
            throw new BadRequestHttpException('no_token');
        }
        if ($token->isExpired()) {
            throw new BadRequestHttpException('expired_token');
        }

        /** @var MultiLoginUserInterface $user */
        $user = $this->userManager->getUserRepository()->findOneBy(['id' => $token->getUserId()]);
        if (!$user) {
            throw new BadRequestHttpException('no_user');
        }

        $user
            ->setLoginEmail($token->getEmail())
            ->setLoginEmailValidated(true);

        if ($data->getPassword() && $data->getPassword() === $data->getPassword2()) {
            $user->setPassword($this->userPasswordEncoder->encodePassword($user, $data->getPassword()));
        }

        $this->entityManager->remove($token);
        $this->entityManager->flush();

        return $this->successHandler->handleAuthenticationSuccess($user);
    }

    /**
     * @param Request $request
     * @return JWTAuthenticationSuccessResponse|Response
     */
    public function resetPassword(Request $request)
    {
        /** @var ResetPasswordReqDto $data */
        $data = $this->dtoSerializer->deserializeBody($request, ResetPasswordReqDto::class);

        if ($data->getToken()) {
            $token = $this->entityManager->getRepository(PasswordResetToken::class)->findOneBy(['id' => $data->getToken()]);
            if (!$token) {
                throw new BadRequestHttpException("no_token");
            }
            if ($token->isExpired()) {
                throw new BadRequestHttpException("token_expired");
            }
            /** @var MultiLoginUserInterface $user */
            $user = $userPhone = $this->userManager->getUserRepository()->findOneBy(['id' => $token->getUserId()]);
            if (!$user) {
                throw new BadRequestHttpException("no_user");
            }
            $user->setPassword($this->userPasswordEncoder->encodePassword($user, $data->getPassword()));
            $this->entityManager->remove($token);
            $this->entityManager->flush();
            return $this->successHandler->handleAuthenticationSuccess($user);
        } else {
            $userPhone = null;
            $userEmail = null;

            $userRepository = $this->userManager->getUserRepository();
            if ($data->getPhone()) {
                $userPhone = $userRepository->findOneBy(['loginPhone' => $data->getPhone()]);
            }
            if ($data->getEmail()) {
                $userEmail = $userRepository->findOneBy(['loginEmail' => $data->getEmail()]);
            }
            if (!$userPhone && !$userEmail) {
                throw new BadRequestHttpException("no_user");
            }
            if ($userPhone && $userEmail && $userEmail !== $userPhone) {
                throw new BadRequestHttpException("email_phone_mismatch");
            }
            /** @var MultiLoginUserInterface $user */
            $user = $userEmail ?? $userPhone;

            $this->tokenManager->deleteExpiredAndPreviousTokensForUser($user->getId(), PasswordResetToken::class);
            $token = $this->tokenManager->createPasswordResetToken($user->getId());

            $this->entityManager->persist($token);
            $this->entityManager->flush();

            if ($data->getPhone()) {
                $sms     = new TemplatedSms();
                $context = [
                    'token' => $token
                ];
                if ($data->getPhoneResetUrl()) {
                    $context['resetUrl'] = $data->getPhoneResetUrl() . "?" . http_build_query(['token' => $token->getId()]);
                }
                $sms->setTo($data->getPhone())
                    ->setTextTemplate('@SungazerUser/sms/password_reset_request.twig')
                    ->setContext($context);
                $this->smsSender->send($sms);
            }

            if ($data->getEmail()) {
                $context = [
                    "token" => $token
                ];
                if ($data->getEmailResetUrl()) {
                    $context['resetUrl'] = $data->getEmailResetUrl() . "?" . http_build_query(['token' => $token->getId()]);
                }
                $this->emailHelper->loadEmailFromTemplateAndSend($data->getEmail(), '@SungazerUser/emails/password_reset_request.html.twig', $context);
            }
            return new Response();
        }
    }


}
