<?php


namespace Sungazer\Bundle\UserBundle\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class VerificationReqDto
{
    /**
     * @var string | null
     */
    private $password;
    /**
     * @var string | null
     * @Assert\Expression(expression="this.getPassword() == this.getPassword2()", message="passwords must match")
     */
    private $password2;
    /**
     * @var string
     * @Assert\NotBlank(allowNull=false)
     */
    private $token;

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return VerificationReqDto
     */
    public function setPassword(?string $password): VerificationReqDto
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword2(): ?string
    {
        return $this->password2;
    }

    /**
     * @param string|null $password2
     * @return VerificationReqDto
     */
    public function setPassword2(?string $password2): VerificationReqDto
    {
        $this->password2 = $password2;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return VerificationReqDto
     */
    public function setToken(string $token): VerificationReqDto
    {
        $this->token = $token;
        return $this;
    }

}