<?php


namespace Sungazer\Bundle\UserBundle\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class ResetPasswordReqDto
{
    /**
     * @var string | null
     */
    private $email;
    /**
     * @var string | null
     */
    private $phone;
    /**
     * @var string | null
     */
    private $token;
    /**
     * @var string | null
     */
    private $password;
    /**
     * @var string | null
     * @Assert\Expression(expression="this.getPassword() == this.getPassword2()", message="passwords must match")
     */
    private $password2;
    /**
     * @var string | null
     */
    private $phoneResetUrl;
    /**
     * @var string | null
     */
    private $emailResetUrl;

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return ResetPasswordReqDto
     */
    public function setEmail(?string $email): ResetPasswordReqDto
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return ResetPasswordReqDto
     */
    public function setPhone(?string $phone): ResetPasswordReqDto
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string|null $token
     * @return ResetPasswordReqDto
     */
    public function setToken(?string $token): ResetPasswordReqDto
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return ResetPasswordReqDto
     */
    public function setPassword(?string $password): ResetPasswordReqDto
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword2(): ?string
    {
        return $this->password2;
    }

    /**
     * @param string|null $password2
     * @return ResetPasswordReqDto
     */
    public function setPassword2(?string $password2): ResetPasswordReqDto
    {
        $this->password2 = $password2;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhoneResetUrl(): ?string
    {
        return $this->phoneResetUrl;
    }

    /**
     * @param string|null $phoneResetUrl
     * @return ResetPasswordReqDto
     */
    public function setPhoneResetUrl(?string $phoneResetUrl): ResetPasswordReqDto
    {
        $this->phoneResetUrl = $phoneResetUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmailResetUrl(): ?string
    {
        return $this->emailResetUrl;
    }

    /**
     * @param string|null $emailResetUrl
     * @return ResetPasswordReqDto
     */
    public function setEmailResetUrl(?string $emailResetUrl): ResetPasswordReqDto
    {
        $this->emailResetUrl = $emailResetUrl;
        return $this;
    }

}